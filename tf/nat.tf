resource "aws_nat_gateway" "johnnat1a" {
  allocation_id = aws_eip.nat1.id
  subnet_id     = aws_subnet.web-subnet-1.id

  tags = {
    Name = "gw NAT1"
  }

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.igw.id]
}

resource "aws_nat_gateway" "johnnat1b" {
  allocation_id = aws_eip.nat2.id
  subnet_id     = aws_subnet.web-subnet-2.id

  tags = {
    Name = "gw NAT2"
  }

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.igw.id]
}

# Create Private layber route table
resource "aws_route_table" "private1a" {
  vpc_id = aws_vpc.my-vpc.id


  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.johnnat1a.id
  }

  tags = {
    Name = "NatRouteA"
  }
}

resource "aws_route_table" "private1b" {
  vpc_id = aws_vpc.my-vpc.id


  route {
    cidr_block = var.cidrblock
    gateway_id = aws_nat_gateway.johnnat1b.id
  }

  tags = {
    Name = "NatRouteB"
  }
}

# Create Private Subnet association
resource "aws_route_table_association" "Private1a" {
  subnet_id      = aws_subnet.application-subnet-1.id
  route_table_id = aws_route_table.private1a.id
}

resource "aws_route_table_association" "Private1b" {
  subnet_id      = aws_subnet.application-subnet-2.id
  route_table_id = aws_route_table.private1b.id
}