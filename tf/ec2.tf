
#Create EC2 Instance
resource "aws_instance" "Bastion" {
  ami                    = var.ami_type
  instance_type          = var.instace_type
  availability_zone      = var.az[0]
  vpc_security_group_ids = [aws_security_group.webserver-sg.id]
  subnet_id              = aws_subnet.web-subnet-1.id
  user_data              = file("install_apache.sh")

  tags = {
    Name = "Bastion Server"
  }

}

resource "aws_instance" "Appserver1" {
  ami                    = var.ami_type
  instance_type          = var.instace_type
  availability_zone      = var.az[0]
  vpc_security_group_ids = [aws_security_group.appserver-sg.id]
  subnet_id              = aws_subnet.application-subnet-1.id
  user_data              = file("install_apache.sh")

  tags = {
    Name = "Dev Server1"
  }

}

resource "aws_instance" "Appserver2" {
  ami                    = var.ami_type
  instance_type          = var.instace_type
  availability_zone      = var.az[1]
  vpc_security_group_ids = [aws_security_group.appserver-sg.id]
  subnet_id              = aws_subnet.application-subnet-2.id
  user_data              = file("install_apache.sh")

  tags = {
    Name = "Dev Server2"
  }

}