# Create a VPC
resource "aws_vpc" "john-vpc" {
  cidr_block = var.cidrblock
  tags = {
    Name = var.vpcname
  }
}
