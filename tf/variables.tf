variable "cidrblock" {
    type    = string
    default = "10.0.0.0/16"
}

variable "vpcname" {
  type    = string
  default = "DEMO-VPC"
}

variable "region" {
    type    = string
    default = "us-east-1"
}

variable "ami_type" {
    type        = string
    default     = "ami-0ff8a91507f77f867"
    description = "AMI for linux ec2 instace"
}

variable "instace_type" {
    type    = string
    default = "t2.micro"
}

variable "az" {
    type    = list
    default = ["us-east-1a", "us-east-1b"]
}

variable "vpc_id" {
    type    = string
    default = "aws_vpc.john-vpc.id"
}

variable "sg_protocols" {
    type = list(object({
        from_port = number
        to_port   = number
        protocol  = string
        }))
    }