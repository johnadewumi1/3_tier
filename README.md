## Terraform code to deploy three-tier architecture on AWS
What is three-tier architecture?
Three-tier architecture is a well-established software application architecture that organizes applications into three logical and physical computing tiers: the presentation tier, or user interface; the application tier, where data is processed; and the data tier, where the data associated with the application is stored and managed.

## What is terraform?
Terraform is an open-source infrastructure as code software tool created by HashiCorp. Users define and provision data center infrastructure using a declarative configuration language known as HashiCorp Configuration Language, or optionally JSON.

##Prerequisites
* Install ***Terraform***
* Install the ***AWS CLI***
* Sign up for an ***AWS Account***
* Your preferred ***IDE*** (I used Visual Studio Code) 

#### Problem Statement
1. One virtual network tied in three subnets.
2. Public subnet will have one virtual machine while Private subnet will have one virtual machine and Database layer with a **MySQL RDS**
3. The virtual machine in the public layer is the bation host-> allow inbound traffic from internet only.
4. The virtual machine in the public layer is the bation host is tha app server and its reading it data from the database.
5. App can connect to database and database can connect to app but database cannot connect to web.
**Note: Keep main and variable files different for each component**

## Solution
#### The Terraform resources will consists of following structure
├── main.tf                   // The primary entrypoint for terraform resources.
├── vars.tf                   // It contain the declarations for variables.
├── output.tf                 // It contain the declarations for outputs.

# Deployment
## teps
#### **Step 0** terraform init

used to initialize a working directory containing Terraform configuration files

#### **Step 1** terraform plan

used to create an execution plan

#### **Step 2** terraform validate

validates the configuration files in a directory, referring only to the configuration and not accessing any remote services such as remote state, provider APIs, etc

#### **Step 3** terraform apply

used to apply the changes required to reach the desired state of the configuration

##Clean Up
1. To delete our infrastructure run terraform destroy . When prompted type Yes. This command will delete all the infrastructure that we created.

**Congratulations on creating a Three-Tier AWS Architecture!**